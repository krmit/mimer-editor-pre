# Att göra för editor

**Massor**

## Första test version

- !Server in background med express
- !Server som tar emot medelande via name pipe.
- !Cli app
- !Med Mirror editor
- !Öppna fil.
- !Configuera med en fil.
- !Spara kommando
- !Se till att Code mirror har bra configuration och du kan använda alla funktioner.
- !Lägg till en prettyfiger vid save.
- !Lägg till autmatisk liner
- !Inför Autocomplete.
- !Gör min egna keymap.
- !Skapa ett git arkiv.

## Snart

- !Gör så prettier körs ifrån en browser.
- !Gör så att vi alltid startar på rätt ställe i koden efter save.
- !Få en find funktion att fungera
- !Sätt title på tab till filens namn.
- !Gör så att programmet kan spara keys om server går ned.
- !Gör en customise hinter funktion med vanliga js commandon.
- !Få keymappen att fungera som den ska.

## Gör en bättre web version

- !Kolla upp hur man gör i ett större projekt med base url.
- !Gör så vi inte användet cjdns för jshint utan npm istället.
- !Gör så att alla libs hämtas ifrån node_module mapparna direkt.
- !Gör så att web delen kan användas självständigt.
- !Gör så vi ha en web vraiant som är lätt att embeda i andra projekt som är statiska.
- !Gör en css fil som stylar programmet.

## Logs

- !Updatera log systemet så det fungerar.
- !Kontrollera att det ger snygga utskrifter gneom att göra ett demo.
- !Inför jsome för json utskrifter.
- !Se till att det är skillna på log och error.
- !Gör en server och en client version.
- !Importera det i både server och client.
- !Gör om alla utskrifter i servern till att använda log systemet.
- !Gör om utskriften i clienten så den använder log systemet.

## Konfiguering: Flera varianter, skärskilt markdown.

- !Fixa konfigueringen.
- !Gör så att den själv skapar en default config fil om ingen finns.
- !Gör så konfigurationer till editor läses på ett annat sätt än via url.
- !Gör så att vi alltid faller tillbacka på default options.
- !Gör så att hint, lint med mera ställs in med hjälp av options.
- !Göt så att olika konfigueringar laddas beronde på fil typ.
- !Gör än addon loader som laddar moduler utifrån en lista på moduler som ska laddas.
- !Se till att rätt modul laddads beronde på fil typ och configuering.
- !Se till att detta fungerar för Markdown.
- !Se till att detta fungerar för txt.

## Many files

- !Refactora clienten så det går lätt att lägga till filer i en loop.
- !Använd globals för att få en lista med filnamn.
- !Lägg till många filer.
- !Gör en websida flera tab i webbläsaren program och sdan byter ut sig själv.
- !Gör så denna sida skickas från servern.
- !Gör så att editor skriver ut en länk till en sida som öppnar alla sidor.
- !Gör inställningar för html filer.
- !Gör inställningar för json.

## Inför första versionen med Valhall

- !Gör en funktion som om det inte är en redan definerad typ, gör det till typen "txt"
- !Förbättra package filen.
- !Skriv ett installtions script som gör att programmet lätt installeras på en ny dator.
- !Gör så setver startas automatiskt när klienten körs.
- !Gör ett komando som automatisk stänger av servern.
- !Gör så att programmet kan skapa alla filer vid instalaltion.
- !Fixa till all kod med hjälp av editor.

## Testa!

- Sätt upp en editor på en server som jag kan testa på från vilken dator som helst.
- Testa i chrome.
- Testa i winbdows med chrome.
- Mera testning
- Fixa bug att inget sparas i text mode.

## Mera

- Skriv en kort insruktion i ett google drive element om hur den används
- Lägg till en REAME.md till projektet.
- Se till att mode har support för ärvda egenskaper.
- Fixa htmlmix
- Se till att mode har support för extra egenskaper
- Fixa typescript
- Fixa json

## Bugar

# Första riktiga versionen

## Diverse

- Fixa till stylen, störe text men inte för lång max editor storlek.
- Se till att editor alltid har max storlek även om filen är mindre.
- Gör så det syns om en fil har ändringar som inte är sparade.

## Import med esd

- Gör om så att vi använder packet esd
- Gör om alla interna packet så de använder import.

## Logger

- Ta in logger functionalitet ifrån andra packet.
- Gör globala options.
- Gör exempel på många flexibla options.
  - Gör så att loggar kan spara till filer.
  - Gör så att web gör en standar logg per default. Den som är nu som alternativ.
- Dela upp logger på funktioner istället.
- Skriv om all kod utifrån ovan.
- Gör så logger funkar med import för både web och cli
- Gör om logger till ett eget packet.
- Gör en view app under tools som använder view funktionen för web.
- Se om du kan använda reqiure från ett icke esd packet.
- Byt color packet till något mindre controversielt. Eventuellt mitt eget.
- Skriv en kort README
- Publisera packet under mimer på npm.

## Bättre error handling

- Kan inte läsa dirs. _Error: EISDIR: illegal operation on a directory, read_

## Många fler typer

- Gör så att vi får många fler fil typer.
- Fundera igenom konfigurationskoden.
- Gör så att en typ kan hänvisa till en annan typ.
- Gör så att programmet blir "smart" när det gäller att gissa typer.

## CLI option

- Implementera ett framwork för options för cli program.
- Lägg till några enkla som skriver över konfiguering.
- Hjälp options.
- Lägg även till denna till servern.
- En option som tömmer databasen.

## Inför första riktiga version

- Lägg till en README fil med dokumentation av alla viktiga komandon och options.
- Gör ett "view program" för en standar log för webben som ger samma utsende som du har här.
- Updatera log programmet så det ger standarlog.
- Ge en option nför vilka loggar som ska anvädnas standar är default.
- Oransiera upp och namnge variablar bättre i koden.
- Skapa ett nytt git arkiv som ska vara permanent.
- Ladda upp som ett nytt projekt på npm.

# Efter relese ska omvandlas till items.

## Diverse

- Överför alla i denna todo fil till items på gitlab.
- Gör så att om en fil öppnas en gång till fås samma länk som föra gången filen öppnades.
- Kopiera över allt som är bra ifrån playground.
- Gå igenom och se om det finns fler plugin som jag borde bifoga.
- Gör så att jshint installeras med hjälp av npm istället för via cdn. Går inte nu pga säkerhetsproblem. jshint har också en mycket jobbig byggprocess.
- Gör så att även keymap kopieras till konfig mappen.

## TS

- Gör konfiguraitoner fil för ts code
- Skriv ned alla problem det leder till ...
- Gör ett script som automatisk konverterar alla ts filer till js.

## Views

- Designa Views, ettkoncept att editorn kan användas på olika sätt.
- Oranisera upp min code utifrån view.
- Gör så att options bestämmer vilken view som ska användas.
- Gör en standar views
- Gör en view för om editorn bara ska vara embeded utan backend.

## Tabs

** Fundersam på om detta äns är nödvändigt.**

- Hitta ett bra biblotek för tabs.
- Importera detta genom npm.
- Skapa en tab view
- Se till att den har flera olika tabbar med hjälp av lib.
- Se till så att varje tab har en instans av editorn.
- Se till så att man manuelt kan införa flera filer.
- Gör så att tabbar har rätt namn.
- Gör så man kan ta bort en tab.
- Gör så man kan flytta på tabbar.
- Göt så att när man byter tab så byts titln på sidan.
- Gör så att options får options för tabbar.
- Ifrån CLI kommandot ska du kunna öpnna flera filer som hamnar i samma view mwn i olika tabs tab.

## Preview

- Gör en function prevview som öppnar en ny tab och visar något där.
- Gör en icon för preview.
- Gör en autoload som autom automatisk uppdaterar en fil om något ändras.
- Gör en preview för markdown med hjälp av https://github.com/evilstreak/markdown-js

## Language Tools Hinter

- Gör en simple linter plugin. Baserad på: https://github.com/codemirror/CodeMirror/blob/master/addon/lint/json-lint.js
- Ta reda på hur det fungerar med options och linter, hitta något bra.
- Fundera hur använda Language Tools externt, gör ett enkelt exempel se:
- Koppla ihopp detta till Language Tools server
- Testa med några texter och fixa små buggar.
- Gör så att välja språk blir en option.
- Gör så att language server blir en option.
- Ge bra shortcuts i editorn, gör alltid spellcheck vid save till exempel.
- Skriv en readme.
- Kompletera den med bra installations instruktioner.
- Publisera på gitlab.
- Fixa till package.json filen så den blir riktigt bra.
- Publisera på npm.

## Kanske

- Gör reklam för mitt pgrogram på codemirrors hemsida.
- Gör type-js som ett alternativ backend.
- Tesata med andra servar för language tools.

## Nätverksförbättringar

- Gör kommunikationen mellan server och klient mer effektiv. Minska antalet get request.

## Förenkla GUI

- Kanppar med vanliga funktioner
- Menyer

## Dokumenthantering

- Gör så att visa dokumment är read-only.
- Gör så att visa dokument är dir också. Dessa kan öppna andra dokument.

## Säkerhet

- Se till att man bara kan läsa ifrån localhost per default.
- Tillåt läsning bara ifrån visa IP adresser.
- Gör en rekomendation om att bara köra programmet som en användare med låg behörighet.

# Långt i frmatiden

## Diverse

- En kompilerad miniversion av editorn.

## Mer med Tabs

- Gör så att varje tab har en icon.
- Gör så att namnet på tab minskar beronde på storleken på tab labeln.

## Med filhantering

- Kan öppna en mapp och visa filerna i denna map.
- Hitta ett projekt som skapar bra iconer av filer och mappar.

## Sniper bibliotek.

- template-hint-plugin - Will add the posibility to add a template in the code throu hint. Titta hur "CodeMirror-XQuery" gjorde.

## Bra hinter

- mix-hinter - Will mix diffrent hinter functions to one. https://stackoverflow.com/questions/19244449/codemirror-autocomplete-custom-list
- smart-hint-plugin - will learn what hint you normal chose. Very simple algorithm.
- good-js-hinter - Baserad på parsning av kod och eventuella bibliotek.
