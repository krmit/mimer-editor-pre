#!/usr/bin/node

const colors = require("colors");

const fs = require("fs-extra");
const util = require("util");
const tail = require("tail");
const path = require("path");
const os = require("os");
const execSync = require("child_process").execSync;
const spawn = require("child_process").spawn;
const { logger } = require("./lib/logger.js");
const log = new logger(5);

const program_name = "mimer-editor";
const path_config_dir = `${os.homedir()}/.config/mimer-editor`;
const path_in = `${path_config_dir}/${program_name}-server`;
const path_out = `${path_config_dir}/${program_name}-client`;

let text = execSync("ps -e");
if (!text.includes("med-server")) {
  log.warn("Server is not runing,starting server.");
  if (!fs.existsSync(path_config_dir)) {
    fs.mkdirSync(path_config_dir);
  }
  const log_path = `${path_config_dir}/server.log`;
  out = fs.openSync(log_path, "a");
  err = fs.openSync(log_path, "a");
  spawn("med-server", {
    stdio: ["ignore", out, err],
    detached: true
  }).unref();
  log.info(
    "The server is started, but the editor must be terminated before it can be used. Please try again."
  );
  process.exit(-1);
}

if (process.argv[2] === "-q") {
  fs.appendFileSync(path_in, "$$$quit\n");
  log.warn("The stoping the server.");
  process.exit(-1);
}

log.trace(process.argv.slice(2));
let file_list = [];
for (let file of process.argv.slice(2)) {
  log.trace(path.join(process.cwd(), file));
  file_list.push(path.join(process.cwd(), file));
}

fs.appendFile(path_in, JSON.stringify(file_list) + "\n");

const editor_out = new tail.Tail(path_out);
editor_out.on("line", dataText => {
  log.trace(dataText);
  const data = JSON.parse(dataText);
  for (const link of data) {
    log.log("open: " + link[0] + " on " + link[1]);
  }
  editor_out.unwatch();
});
