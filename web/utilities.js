"use strict";

//Frome https://stackoverflow.com/questions/10609511/javascript-url-parameter-parsing
export function parseGet() {
  var get = {
      push: function(key, value) {
        var cur = this[key];
        if (cur.isArray) {
          this[key].push(value);
        } else {
          this[key] = [];
          this[key].push(cur);
          this[key].push(value);
        }
      }
    },
    search = document.location.search,
    decode = function(s, boo) {
      var a = decodeURIComponent(s.split("+").join(" "));
      return boo ? a.replace(/\s+/g, "") : a;
    };
  search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function(a, b, c) {
    if (get[decode(b, true)]) {
      get.push(decode(b, true), decode(c));
    } else {
      get[decode(b, true)] = decode(c);
    }
  });
  return get;
}

//From https://stackoverflow.com/questions/950087/how-do-i-include-a-javascript-file-in-another-javascript-file answer by Alex
// My modification to make it work with promise and import.
export function importOldJS(url) {
  return new Promise(function(resolve, reject) {
    // Adding the script tag to the head as suggested before
    const head = document.getElementsByTagName("head")[0];
    const script = document.createElement("script");
    script.type = "text/javascript";
    script.src = url;

    // Then bind the event to the callback function.
    // There are several events for cross browser compatibility.
    script.onreadystatechange = resolve;
    script.onload = resolve;

    // Fire the loading
    head.appendChild(script);
  });
}

// From https://stackoverflow.com/questions/574944/how-to-load-up-css-files-using-javascript answer by sandstrom
export function importStyle(url) {
  return new Promise((resolve, reject) => {
    let link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.onload = function() {
      resolve();
    };
    link.href = url;

    let headScript = document.querySelector("script");
    headScript.parentNode.insertBefore(link, headScript);
  });
}

export function getRoot() {
  //Find the root link so loading will be from right place
  let document_root_list = document.URL.split("?")[0].split("/");
  document_root_list.pop();
  return document_root_list.join("/");
}
