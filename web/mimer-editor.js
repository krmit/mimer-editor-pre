//<script src="http://cdnjs.cloudflare.com/ajax/libs/jshint/2.9.5/jshint.min.js"></script>
/*<script src="prettier/parser-babylon.js"></script>
 */
"use strict";
import {
  createSave,
  createAutocomplete,
  createHintAnyword
} from "../web/commands.js";
import { importOldJS, importStyle, getRoot } from "../web/utilities.js";
import { logger } from "../lib/logger-web.js";
const log = new logger(7);

//Frome https://stackoverflow.com/questions/10609511/javascript-url-parameter-parsing

export default function editor(userConfig) {
  // Default configurations
  const default_config = {
    theme: "dracula",
    path: "",
    url: "",
    keymap: "mimer",
    name: "untitled"
  };

  // Get root url for editor files
  let document_root = getRoot();
  // Load codemirror
  Promise.all([
    importOldJS(document_root + "/node_modules/codemirror/lib/codemirror.js"),
    fetch(document_root + "/config/config.json").then(
      p => p.json(),
      () => {
        log.warn("No user config.");
        return {};
      }
    )
  ]).then(first_load => {
    // Construct configurations
    log.debug("Type of file: " + userConfig.type);
    // Creat config base on default config for editor, config from server, config from user.
    const editor_config = Object.assign({}, default_config, first_load[1]);
    const lang_config = editor_config.types[userConfig.type];
    editor_config.types = null;
    const config = Object.assign({}, editor_config, lang_config, userConfig);
    log.debug("Config is created.");
    log.trace(config);
    // Load all addons to codemirror
    return Promise.all([
      // Load theme
      importStyle(
        document_root +
          "/node_modules/codemirror/theme/" +
          config.theme +
          ".css"
      ),
      importStyle(
        document_root + "/node_modules/codemirror/lib/codemirror.css"
      ),
      importStyle(document_root + "/node_modules/codemirror/theme/zenburn.css"),
      importStyle(
        document_root + "/node_modules/codemirror/addon/lint/lint.css"
      ),
      importStyle(
        document_root + "/node_modules/codemirror/addon/hint/show-hint.css"
      ),
      importStyle(
        document_root + "/node_modules/codemirror/addon/dialog/dialog.css"
      ),
      importOldJS(document_root + "/node_modules/prettier/standalone.js"),

      // Load formater.
      config.formatter !== undefined
        ? importOldJS(
            document_root +
              "/node_modules/prettier/parser-" +
              config.formatter.config.parser +
              ".js"
          )
        : Promise.resolve(null),
      importOldJS(
        document_root + "/node_modules/codemirror/addon/hint/show-hint.js"
      ),

      // Load language mode
      config.mode !== "text/plain"
        ? importOldJS(
            document_root +
              "/node_modules/codemirror/mode/" +
              config.mode +
              "/" +
              config.mode +
              ".js"
          )
        : Promise.resolve(null),

      // Load hinter
      config.hinter !== undefined
        ? importOldJS(
            document_root +
              "/node_modules/codemirror/addon/hint/" +
              config.hinter.name +
              "-hint.js"
          )
        : Promise.resolve(null),

      // Load linter for language
      config.linter !== undefined
        ? importOldJS(
            document_root + "/node_modules/codemirror/addon/lint/lint.js"
          )
        : Promise.resolve(null),
      config.linter !== undefined
        ? importOldJS(
            document_root +
              "/node_modules/codemirror/addon/lint/" +
              config.linter.name +
              ".js"
          )
        : Promise.resolve(null),
      config.mode === "javascript"
        ? importOldJS(document_root + "/node_modules/jshint/dist/jshint.js")
        : Promise.resolve(null),
      // Good functions for all types
      importOldJS(
        document_root + "/node_modules/codemirror/addon/edit/matchbrackets.js"
      ),
      importOldJS(
        document_root + "/node_modules/codemirror/addon/search/search.js"
      ),
      importOldJS(
        document_root + "/node_modules/codemirror/addon/search/searchcursor.js"
      ),
      importOldJS(
        document_root + "/node_modules/codemirror/addon/search/jump-to-line.js"
      ),
      importOldJS(
        document_root + "/node_modules/codemirror/addon/dialog/dialog.js"
      )
    ]).then(function() {
      return Promise.all([
        importStyle(document_root + "/web/style.css"),
        fetch(document_root + "/config/keymaps.json").then(p => p.json()),
        fetch("./"+config.id).then(p => p.text())
      ]).then(function(data) {
        const keyMap = CodeMirror.keyMap;
        keyMap.mimer = data[1];

        let config_editor = {
          lineNumbers: true,
          mode: config.mode,
          matchBrackets: true,
          theme: config.theme,
          keyMap: config.keymap
        };

        log.debug("Editor config created.");
        log.trace(config_editor);

        if (config.linter) {
          Object.assign(config_editor, {
            gutters: ["CodeMirror-lint-markers"],
            lint: config.linter.config
          });
        }

        const editor = CodeMirror.fromTextArea(
          document.getElementById("code"),
          config_editor
        );

        // Commands
        CodeMirror.commands.save = createSave(config, editor);

        //# Hint with anyword
        CodeMirror.hint.anyword = createHintAnyword(CodeMirror.hint.anyword);
        CodeMirror.commands.autocomplete = createAutocomplete(
          CodeMirror.hint.anyword
        );

        editor.setValue(data[2]);
        return editor;
      });
    });
  });
}
