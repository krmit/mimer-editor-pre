"use strict";

export function createAutocomplete(autocomplete) {
  return function(cm) {
    cm.showHint({ hint: autocomplete });
  };
}

export function createSave(config, editor) {
  return function() {
    console.log("Saving data with id " + config.name);

    const position = editor.doc.getCursor();

    let nice_code;
    if (config.formatter) {
      config.formatter.config.plugins = prettierPlugins;
      nice_code = prettier.format(editor.getValue(), config.formatter.config);
    } else {
      nice_code = editor.getValue();
    }

    editor.setValue(nice_code);
    editor.doc.setCursor(position);
    fetch(config.url, {
      method: "POST",
      body: nice_code
    });
  };
}

const dir_js_word = [
  "function (){\n\n}",
  "const",
  "let",
  "for(let i = 0;i < .length;i++){\n\n}",
  'console.log("");',
  "if(===){\n\n}",
  '= require("");',
  'require("");'
];

export function createHintAnyword(originalHint) {
  return function(cm) {
    let word = "";
    let cursor = editor.getCursor();
    let end = cursor.ch;
    let start = end;

    let line = editor.getLine(cursor.line);
    while (true) {
      let ch = line.charAt(end);
      if (" \t\n\r\v".indexOf(ch) > -1 && start != 0) {
        start--;
      } else {
        word = line.slice(start, end);
        break;
      }
    }

    let result = originalHint(cm);
    if (result === undefined) {
      result = { from: start, to: end, list: [] };
    }

    let filter_words = dir_js_word.filter(element => element.startsWith(word));
    result.list = filter_words.concat(
      result.list.filter(function(itelom) {
        return filter_words.indexOf(item) < 0;
      })
    );
    return result;
  };
}
