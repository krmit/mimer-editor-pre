/*
 *   Copyright 2019 Magnus Kronnäs
 *
 *   This file is part of easy-web-framework.
 *
 *   logger.js is free software: you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software 
 *   Foundation, either version 3 of the License, or (at your option) any later 
 *    version.
 *
 *   easy-web-framework is distributed in the hope that it will be useful, but WITHOUT ANY 
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along with 
 *   easy-web-framework. If not, see http://www.gnu.org/licenses/.
 */
"use strict";

//const colors = require("colors");
//const moment = require("moment");
//import {moment} from '../node_modules/moment/src/moment.js'
//const pretty= require("jsome").getColoredString;
const pretty = function(obj){return JSON.stringify(obj, null, 2)};
//const useragent = require('useragent');

//function logger(level) {
export function logger(level) {
    const log_print = function(tag, msg) {
        let tag_color = function(msg){return msg;};
        let msg_color = function(msg){return msg;};
        let console_metod = console.log;
        switch(tag) {
        case "fatal":
            //tag_color = colors.red.bold;
            //msg_color = colors.bold;
            console_metod = console.error;
            break;
        case "error":
            //tag_color = colors.red;
            console_metod = console.error;
            break;
        case "warn":
            //tag_color = colors.yellow;
            console_metod = console.warn;
            break;
        case "info":
            //tag_color = colors.green;
            console_metod = console.info;
            break;
        case "debug":
            //tag_color = colors.green;
            console_metod = console.debug;
            break;
        case "trace":
            //tag_color = colors.green;
            console_metod = console.debug;
            break;
        default:
            //tag_color = colors.green;
        }

        if(typeof msg === "string" || typeof msg === "boolean" || typeof msg === "number") {
            console_metod("["+tag_color(tag) + "]"+ " ".repeat(6-tag.length) + msg_color(msg));
        }
        else  {
            // Needed good pretty printing of json and object because of circular objects.
            console.log("["+tag_color(tag) + "]\n" + msg_color(pretty(msg)));
        }
    };


    this.trace = function(msg) {};
    if(level > 6) {
        this.trace = function (msg) {log_print("trace", msg);};
    }

    this.debug = function(msg) {};
    if(level > 5) {
        this.debug = function (msg) {log_print("debug", msg);};
    }

    this.info = function(msg) {};
    if(level > 4) {
        this.info = function (msg) {log_print("info", msg);};
    }

    this.log = function(msg) {};
    if(level > 3) {
        this.log = function (msg) {console.log(msg);};
    }

    this.warn = function(msg) {};
    if(level > 2) {
        this.warn = function (msg) {log_print("warn", msg);};
    }

    this.error = function(msg) {};
    if(level > 1) {
        this.error = function (msg) {log_print("error", msg);};
    }

    this.fatal = function(msg) {};
    if(level > 0) {
        this.fatal = function (msg) {log_print("fatal", msg);};
    }
    
    //Bad solution, better to give standar log and then have a view program if needed.
/*    this.web = function (req) {
		 const agent = useragent.parse(req.headers['user-agent']);
		 this.log(("["+moment().format("YYYY-MM-DD HH:mm:ss")+"] ").green+
                req.ip.bold+" "+req.protocol.red + " " + req.method.green 
                + " " +req.originalUrl.yellow+" "+agent.toAgent().green+" "+
                agent.os.toString().bold+" "+agent.device.toString().yellow);
			}*/
}
