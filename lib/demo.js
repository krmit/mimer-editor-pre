#!/usr/bin/node
"use strict";
const { logger } = require("./logger.js");

const log = new logger(7);

log.log("Test!");

log.fatal("Very very bad!");
log.error("Very bad!");
log.warn("Could be bad!");
log.info("For your information!");
log.debug("Something is wrong!");
log.trace("Where is the bug?!");

log.log(true);
log.info(42);
log.info(["This", "is", "a", "test"]);
log.info({ data: [1, 2, 3], id: 1 });

let req = {
  ip: "10.10.10.10",
  protocol: "https",
  method: "post",
  originalUrl: "https://www.dn.se",
  headers: {
    "user-agent":
      "Mozilla/5.0 (iPad; U; CPU OS 3_2_1 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Mobile/7B405"
  }
};
log.web(req);
