#!/usr/bin/node
/*
 *   Copyright 2019 Magnus Kronnäs
 *
 *   This file is part of easy-web-framework.
 *
 *   logger.js is free software: you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any later
 *    version.
 *
 *   easy-web-framework is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License along with
 *   easy-web-framework. If not, see http://www.gnu.org/licenses/.
 */
"use strict";

const express = require("express");
const colors = require("colors");
const process = require("process");
const nanoid = require("nanoid");
const fs = require("fs-extra");
const path = require("path");
const tail = require("tail");
const yaml = require("js-yaml");
const os = require("os");
const { logger } = require("../lib/logger.js");

//const stdin = process.openStdin();
const log = new logger(7);

let database = {};
const program_name = "mimer-editor";
const program_dir = path.join(__dirname, "..");
const path_config_default = `${program_dir}/config/default.yml`;
const path_config_dir = `${os.homedir()}/.config/mimer-editor`;
const path_in = `${path_config_dir}/${program_name}-server`;
const path_out = `${path_config_dir}/${program_name}-client`;
const path_config = `${path_config_dir}/config.yml`;
const path_database = `${path_config_dir}/database.json`;

if (!fs.existsSync(path_config_dir)) {
  fs.mkdirSync(path_config_dir);
}

if (!fs.existsSync(path_database)) {
  fs.writeFile(path_database, "{}");
}

if (!fs.existsSync(path_config)) {
  fs.copySync(`${program_dir}/config/empty-config.yml`, path_config);
}

fs.closeSync(fs.openSync(path_in, "w"));
fs.closeSync(fs.openSync(path_out, "w"));

const editor_in = new tail.Tail(path_in);

fs.readFile(path_database, { encoding: "utf-8" })
  .then(yaml.safeLoad)
  .then(data => {
    log.debug("The database for the program is loaded.");
    log.trace(data);
    database = data;
  });
Promise.all([
  fs.readFile(path_config, { encoding: "utf-8" }).then(yaml.safeLoad),
  fs.readFile(path_config_default, { encoding: "utf-8" }).then(yaml.safeLoad)
])
  .then(data => Object.assign({}, data[1], data[0]))
  .then(config => {
    log.debug("The config of the program is loaded.");
    log.trace(config);
    const port = config.port;
    if(!config.externalPort) {
		 config.externalPort = port;
	}
    const base_url = `${config.protocol}://${config.domain}:${config.externalPort}${config.searchPath}`;

    editor_in.on("line", function(dataText) {
      if (dataText === "$$$quit") {
        log.warn("Stoping server.");
        process.exit(-1);
      } else {
        let result = [];
        const data = JSON.parse(dataText);
        for (const file of data) {
          const key = nanoid();
          database[key] = {
            id: key,
            path: String(file).trim(),
            url: `${base_url}/${key}`,
            urlToEdit: `${base_url}/editor?id=${key}`,
            name: String(file)
              .trim()
              .split("/")
              .slice()
              .pop()
          };
          // Decide type
          const type = database[key].name.split(".").pop();
          if (Object.keys(config.types).indexOf(type) !== -1) {
            database[key].type = type;
          } else {
            database[key].type = "txt";
          }
          log.debug("The file type is: " + database[key].type);

          log.info(
            "Open file: ".bold +
              database[key].path.green +
              " on ".bold +
              database[key].url.green
          );
          result.push([database[key].name, database[key].urlToEdit]);
          log.trace(database[key].url);
        }

        if (result.length !== 1) {
          const key = nanoid();
          database[key] = {
            id: key,
            path: "",
            url: `${base_url}/open/${key}`,
            urlToEdit: `${base_url}/open/${key}`,
            name: "All " + result.length + " files",
            description: result.reduce(
              (text, value) => text + value[0] + " ",
              " "
            ),
            open: Object.assign([], result)
          };
          log.trace(database[key]);
          result.push([database[key].name, database[key].urlToEdit]);
        }

        fs.appendFile(path_out, JSON.stringify(result) + "\n");

        fs.writeFile(path_database, JSON.stringify(database));
      }
    });

    function server_error(req, res) {
	  log.error("404: Not found.");
      res.status(404).send("<h1>Not Found: 404</h1>" + req.url);
    }

    const app = express();

    app.get("/", function(req, res) {
      server_error(req, res);
    });

    // For making log on all request.
    app.use(function(req, res, next) {
      log.web(req);
      next();
    });

    app.use(
      "/editor",
      express.static(path.join(program_dir, "web/editor.html"))
    );
    app.use("/web", express.static(path.join(program_dir, "web/")));
    app.use("/lib", express.static(path.join(program_dir, "lib/")));
    app.use("/config", express.static(path.join(program_dir, "config/")));
    app.use(
      "/node_modules/",
      express.static(path.join(program_dir, "node_modules/"))
    );
    app.get("/config/config.json", function(req, res) {
	  log.trace("Load config");
      res.send(JSON.stringify(config));
    });

    app.get("/info/:fileId", function(req, res) {
	  log.trace("Info about "+req.params["fileId"]);
      const file_key = req.params["fileId"];
      if (database.hasOwnProperty(file_key)) {
        res.send(JSON.stringify(database[file_key]));
      } else {
        server_error(req, res);
      }
    });

    app.get("/open/:fileId", function(req, res) {
	  log.trace("Open "+req.params["fileId"]);
      const file_key = req.params["fileId"];
      if (
        database.hasOwnProperty(file_key) &&
        database[file_key].hasOwnProperty("open")
      ) {
        let open_element_text =
          "<ul>\n" +
          database[file_key].open.reduce(
            (result, value) => result + "<li>" + value[0] + "</li>\n",
            ""
          ) +
          "</ul>\n";
        let open_list = Object.assign([], database[file_key].open);
        let last_element = open_list.pop();
        let open_element_code = open_list.reduce(
          (result, value) =>
            result + 'window.open("' + value[1] + '", "_blank");\n',
          ""
        );
        open_element_code +=
          'window.open("' + last_element[1] + '", "_self");\n';
        res.send(
          `<!doctype html>
  <html>
    <head>
	   <meta charset="utf-8" />
       <title>Launcher</title>
       <script type="module">
       "use strict"
       ${open_element_code}        
         </script>
</head>
<body>
<h1>Lanucher</h1>
${open_element_text}
</body>
</html>
`
        );
      } else {
        server_error(req, res);
      }
    });

    app.get("/:fileId", function(req, res) {
	  log.trace("Get data for "+req.params["fileId"]);
      const file_key = req.params["fileId"];
      if (database.hasOwnProperty(file_key)) {
        fs.readFile(database[file_key].path, { encoding: "utf-8" }).then(data =>
          res.send(data)
        );
      } else {
        server_error(req, res);
      }
    });

    app.post("/:fileId", function(req, res) {
	  log.trace("Sending data for "+req.params["fileId"]);
      const path_to_file = database[req.params["fileId"]].path;
      const wstream = fs.createWriteStream(path_to_file);
      req.pipe(wstream);

      wstream.on("error", err => {
        log.error(path_to_file);
        log.error(err);
      });

      wstream.on("close", () => {
        log.info("Save file: " + path_to_file);
        fs.readFile(path_to_file, { encoding: "utf-8" }).then(data => {
          fs.writeFile(path_to_file, data);
        });
      });
      res.send("ok");
    });

    app.listen(config["port"], () => {
      log.info("Started server on port: " + config["port"]);
      log.debug("Test on URL: http://127.0.0.1:" + config["port"]);
    });
  });
